/* global MozMobileMessageShim */

(function() {
'use strict';

MozMobileMessageShim.init(navigator.mozMobileMessage);
})(self);
